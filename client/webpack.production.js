const path = require( 'path' );
const webpack = require( 'webpack' );
const eslintloader = require('eslint-loader');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlCriticalPlugin = require("html-critical-webpack-plugin");
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');



module.exports = {
	devtool: 'cheap-source-map',

    context: path.resolve( __dirname ),
    entry: {
		app: './wwwroot/app/app.js'
    },
    output: {
        filename: '[name].[chunkhash].js',
        chunkFilename: '[name].[chunkhash].chunk.js',
        path: path.join( __dirname, 'wwwroot/dist/' ),
        publicPath: '/dist/'
    },

    plugins: [
        new webpack.DefinePlugin({
            ENV: JSON.stringify('production'),
            API_URL: JSON.stringify('http://192.168.1.15:3000/shopify?path='),
        }),
        new webpack.optimize.ModuleConcatenationPlugin(),
		new ExtractTextPlugin({
            filename: '[name].[contenthash].css',
			allChunks: true
        }),
        new OptimizeCssAssetsPlugin({
            assetNameRegExp: /\.css$/g,
            cssProcessor: require('cssnano'),
            cssProcessorOptions: { discardComments: { removeAll: true } },
            canPrint: false
        }),
		new HtmlWebpackPlugin({
			title: 'Stitch Test PROD',
			filename: '../index.html',
			template: 'wwwroot/index.ejs',
            inject: 'head'
        }),
        new CleanWebpackPlugin(
            [   // paths to clean
                'wwwroot/tmp-*.css',
                'wwwroot/dist/*.css*',
                'wwwroot/dist/*.js*',
                'wwwroot/index.html'
            ],
            {
                verbose: true,
                exclude: [
                    'wwwroot/dist/app.bundle.js',
                    'wwwroot/dist/vendor.bundle.js'
                ]
            }
        ),
        new HtmlCriticalPlugin({
            base: 'wwwroot',
            src: 'index.html',
            dest: 'index.html',
            inline: true,
            minify: true,
            extract: false,
            width: 480,
            height: 800,
            // ignore font awesome, and background images
            ignore: ['@font-face', /url\(/]
        }),
        new ManifestPlugin({
            'fileName': 'assets.json'
        })
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                enforce: 'pre',
                loader: 'eslint-loader',
                include: path.resolve(__dirname, 'wwwroot/app'),
				exclude: /node_modules/
            },
			{
				test: /\.css$/,
				use: ExtractTextPlugin.extract({
					fallback: 'style-loader',
                    use: 'css-loader'
                })
            }
        ]
    },
    resolve: {
        modules: [
            'node_modules'
        ]
    }
};
