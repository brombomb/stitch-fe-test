# Installation
* npm install
* npm run build-dev

* Use a server of your choice to run the client starting in the root dir of `wwwroot`.  I used a simple node http-server

## About
Uses AngularJS 1.7.2 combined with a webpack 2 build.  Webpack is used to import files, and create bundles for the js. It is also used to watch the files for changes and automatically build the bundles on save. I used bootstrap 4 for some simple styling.  

## Missing features
I coded up inventory saving, but then found it was on a separate API, not too hard ot tackle, but I felt I had enough with the update.

Filtering by dates is custom coded date logic.  I should have used moment or a lighter weight date-fns library to format the date params to shopify API standards.  Who uses +/- TZ instead of an ISO format?  Geeze get with it shopify

Testing - I haven't worked with testing before.  I'd love to be shown how to properly implement testing into my code,  but just haven't had any prior expirience with it.

Orders/Data Vis - I wanted to do something here, but didn't know how to place an order on my shopify store to get data to work with.  
