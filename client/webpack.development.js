const path = require( 'path' );
const webpack = require( 'webpack' );
//const eslintloader = require('eslint-loader');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

module.exports = {
	devtool: 'cheap-eval-source-map',
    context: path.resolve( __dirname ),
    entry: {
		app: './wwwroot/app/app.js'
        /*
        , vendor: [
			'lodash', 
			'jquery', 
			'angular',
			'angular-ui-router',
		],
        */
    },
    output: {
        filename: '[name].bundle.js',
        chunkFilename: '[name].[chunkhash].chunk.js',
        path: path.join( __dirname, 'wwwroot/dist/' ),
        publicPath: 'dist/'
    },

    plugins: [
        new webpack.DefinePlugin({
            ENV: JSON.stringify('development'),
            API_URL: JSON.stringify('http://192.168.1.15:3000/shopify?path='),
        }),
		new ExtractTextPlugin({
            filename: '[name].css',
			allChunks: true
        }),
		new webpack.SourceMapDevToolPlugin({
			filename: '[name].js.map',
			exclude: ['vendor.js']
		}),
		new HtmlWebpackPlugin({
			title: 'Stitch Test',
			filename: '../index.html',
			template: 'wwwroot/index.ejs',
            inject: 'head'
        }),
        new CleanWebpackPlugin(
            [
                'wwwroot/tmp-*.css',
                'wwwroot/dist/*.css*',
                'wwwroot/dist/*.js*',
                'wwwroot/dist/report.html',
            ], 
            {
                verbose: true,
                exclude: [
                    'wwwroot/dist/app.bundle.js',
                    'wwwroot/dist/vendor.bundle.js'
                ]
            }
        ),
    ],
    module: {
        rules: [
            /*{
                test: /\.js$/,
                enforce: 'pre',
                loader: 'eslint-loader',
                include: path.resolve(__dirname, 'wwwroot/app'),
				exclude: /node_modules/
            },*/
			{
				test: /\.css$/,
				use: ExtractTextPlugin.extract({
					fallback: 'style-loader',
                    use: 'css-loader'
                })
            }
        ]
    },
    resolve: {
        modules: [
            'node_modules'
        ]
    }
};
