/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "dist/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */,
/* 1 */
/* no static exports found */
/* all exports used */
/*!******************************************!*\
  !*** ./wwwroot/app/controllers/index.js ***!
  \******************************************/
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("﻿\n\nangular.module('stitchApp')\n.controller('productsController', __webpack_require__(/*! ./products.js */ 7))\n.controller('newController', __webpack_require__(/*! ./new.js */ 5))\n.controller('itemController', __webpack_require__(/*! ./item.js */ 4));\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiMS5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL3d3d3Jvb3QvYXBwL2NvbnRyb2xsZXJzL2luZGV4LmpzP2VhZGEiXSwic291cmNlc0NvbnRlbnQiOlsi77u/J3VzZSBzdHJpY3QnO1xuXG5hbmd1bGFyLm1vZHVsZSgnc3RpdGNoQXBwJylcbi5jb250cm9sbGVyKCdwcm9kdWN0c0NvbnRyb2xsZXInLCByZXF1aXJlKCcuL3Byb2R1Y3RzLmpzJykpXG4uY29udHJvbGxlcignbmV3Q29udHJvbGxlcicsIHJlcXVpcmUoJy4vbmV3LmpzJykpXG4uY29udHJvbGxlcignaXRlbUNvbnRyb2xsZXInLCByZXF1aXJlKCcuL2l0ZW0uanMnKSk7XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3d3d3Jvb3QvYXBwL2NvbnRyb2xsZXJzL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAxXG4vLyBtb2R1bGUgY2h1bmtzID0gMCJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOyIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///1\n");

/***/ }),
/* 2 */
/* no static exports found */
/* all exports used */
/*!****************************************!*\
  !*** ./wwwroot/app/factories/index.js ***!
  \****************************************/
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("﻿\n\nangular.module('stitchApp')\n    .factory('apiFactory', __webpack_require__(/*! ./api.js */ 6));\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiMi5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL3d3d3Jvb3QvYXBwL2ZhY3Rvcmllcy9pbmRleC5qcz9hOTM0Il0sInNvdXJjZXNDb250ZW50IjpbIu+7vyd1c2Ugc3RyaWN0JztcblxuYW5ndWxhci5tb2R1bGUoJ3N0aXRjaEFwcCcpXG4gICAgLmZhY3RvcnkoJ2FwaUZhY3RvcnknLCByZXF1aXJlKCcuL2FwaS5qcycpKTtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vd3d3cm9vdC9hcHAvZmFjdG9yaWVzL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAyXG4vLyBtb2R1bGUgY2h1bmtzID0gMCJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7Iiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///2\n");

/***/ }),
/* 3 */
/* no static exports found */
/* all exports used */
/*!****************************!*\
  !*** ./wwwroot/app/app.js ***!
  \****************************/
/***/ (function(module, exports, __webpack_require__) {

eval("﻿angular.module('stitchApp', ['ui.router'])\n    .config(function ($locationProvider, $stateProvider, $httpProvider) {\n\n        $locationProvider.html5Mode({\n            enabled: true,\n            requireBase: false\n        });\n\n        var states = [\n            {\n                name: 'products',\n                url: '/',\n                templateUrl: 'products.html'\n            },\n            {\n                name: 'product',\n                url: '/product?pid',\n                templateUrl: 'item.html'\n            },\n            {\n                name: 'new',\n                url: '/new',\n                templateUrl: 'new.html'\n            }\n        ];\n\n        states.forEach(function (state) {\n            $stateProvider.state(state);\n        });\n\n    })\n\n__webpack_require__(/*! ./factories */ 2);\n//require('./components');\n__webpack_require__(/*! ./controllers */ 1);\n//require('./filters');\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiMy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL3d3d3Jvb3QvYXBwL2FwcC5qcz9kNTJkIl0sInNvdXJjZXNDb250ZW50IjpbIu+7v2FuZ3VsYXIubW9kdWxlKCdzdGl0Y2hBcHAnLCBbJ3VpLnJvdXRlciddKVxuICAgIC5jb25maWcoZnVuY3Rpb24gKCRsb2NhdGlvblByb3ZpZGVyLCAkc3RhdGVQcm92aWRlciwgJGh0dHBQcm92aWRlcikge1xuXG4gICAgICAgICRsb2NhdGlvblByb3ZpZGVyLmh0bWw1TW9kZSh7XG4gICAgICAgICAgICBlbmFibGVkOiB0cnVlLFxuICAgICAgICAgICAgcmVxdWlyZUJhc2U6IGZhbHNlXG4gICAgICAgIH0pO1xuXG4gICAgICAgIHZhciBzdGF0ZXMgPSBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbmFtZTogJ3Byb2R1Y3RzJyxcbiAgICAgICAgICAgICAgICB1cmw6ICcvJyxcbiAgICAgICAgICAgICAgICB0ZW1wbGF0ZVVybDogJ3Byb2R1Y3RzLmh0bWwnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIG5hbWU6ICdwcm9kdWN0JyxcbiAgICAgICAgICAgICAgICB1cmw6ICcvcHJvZHVjdD9waWQnLFxuICAgICAgICAgICAgICAgIHRlbXBsYXRlVXJsOiAnaXRlbS5odG1sJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBuYW1lOiAnbmV3JyxcbiAgICAgICAgICAgICAgICB1cmw6ICcvbmV3JyxcbiAgICAgICAgICAgICAgICB0ZW1wbGF0ZVVybDogJ25ldy5odG1sJ1xuICAgICAgICAgICAgfVxuICAgICAgICBdO1xuXG4gICAgICAgIHN0YXRlcy5mb3JFYWNoKGZ1bmN0aW9uIChzdGF0ZSkge1xuICAgICAgICAgICAgJHN0YXRlUHJvdmlkZXIuc3RhdGUoc3RhdGUpO1xuICAgICAgICB9KTtcblxuICAgIH0pXG5cbnJlcXVpcmUoJy4vZmFjdG9yaWVzJyk7XG4vL3JlcXVpcmUoJy4vY29tcG9uZW50cycpO1xucmVxdWlyZSgnLi9jb250cm9sbGVycycpO1xuLy9yZXF1aXJlKCcuL2ZpbHRlcnMnKTtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vd3d3cm9vdC9hcHAvYXBwLmpzXG4vLyBtb2R1bGUgaWQgPSAzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOyIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///3\n");

/***/ }),
/* 4 */
/* no static exports found */
/* all exports used */
/*!*****************************************!*\
  !*** ./wwwroot/app/controllers/item.js ***!
  \*****************************************/
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("﻿\n\nitemController.$inject = ['apiFactory', '$location'];\n\nfunction itemController(api, $location) {\n    var ctrl = this;\n\n    var params = $location.search();\n    ctrl.pid = params.pid;\n\n\tctrl.getItem = function () {\n        return api.getItem(ctrl.pid).then(function (response) {\n            ctrl.item = response.data.product;\n        });\n\t};\n\n    ctrl.update = function() {\n        var update = {\n            product: ctrl.item\n        };\n\n        // was going to do inventory but it's be deprecated and moved to a separate API route.\n        return api.updateItem(ctrl.pid, update).then(function (response) {\n            ctrl.item = response.data.product;\n        });\n    };\n\n\n    ctrl.delete = function() {\n        api.removeItem(ctrl.pid).then(function(response) {\n            window.location = '/';\n        });\n    };\n\n    ctrl.init = function () {\n        ctrl.getItem();\n\t};\n\n\t// First Load\n\tctrl.init();\n\n}\n\nmodule.exports = itemController;\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiNC5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL3d3d3Jvb3QvYXBwL2NvbnRyb2xsZXJzL2l0ZW0uanM/YzNiNCJdLCJzb3VyY2VzQ29udGVudCI6WyLvu78ndXNlIHN0cmljdCc7XG5cbml0ZW1Db250cm9sbGVyLiRpbmplY3QgPSBbJ2FwaUZhY3RvcnknLCAnJGxvY2F0aW9uJ107XG5cbmZ1bmN0aW9uIGl0ZW1Db250cm9sbGVyKGFwaSwgJGxvY2F0aW9uKSB7XG4gICAgdmFyIGN0cmwgPSB0aGlzO1xuXG4gICAgdmFyIHBhcmFtcyA9ICRsb2NhdGlvbi5zZWFyY2goKTtcbiAgICBjdHJsLnBpZCA9IHBhcmFtcy5waWQ7XG5cblx0Y3RybC5nZXRJdGVtID0gZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gYXBpLmdldEl0ZW0oY3RybC5waWQpLnRoZW4oZnVuY3Rpb24gKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICBjdHJsLml0ZW0gPSByZXNwb25zZS5kYXRhLnByb2R1Y3Q7XG4gICAgICAgIH0pO1xuXHR9O1xuXG4gICAgY3RybC51cGRhdGUgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgdmFyIHVwZGF0ZSA9IHtcbiAgICAgICAgICAgIHByb2R1Y3Q6IGN0cmwuaXRlbVxuICAgICAgICB9O1xuXG4gICAgICAgIC8vIHdhcyBnb2luZyB0byBkbyBpbnZlbnRvcnkgYnV0IGl0J3MgYmUgZGVwcmVjYXRlZCBhbmQgbW92ZWQgdG8gYSBzZXBhcmF0ZSBBUEkgcm91dGUuXG4gICAgICAgIHJldHVybiBhcGkudXBkYXRlSXRlbShjdHJsLnBpZCwgdXBkYXRlKS50aGVuKGZ1bmN0aW9uIChyZXNwb25zZSkge1xuICAgICAgICAgICAgY3RybC5pdGVtID0gcmVzcG9uc2UuZGF0YS5wcm9kdWN0O1xuICAgICAgICB9KTtcbiAgICB9O1xuXG5cbiAgICBjdHJsLmRlbGV0ZSA9IGZ1bmN0aW9uKCkge1xuICAgICAgICBhcGkucmVtb3ZlSXRlbShjdHJsLnBpZCkudGhlbihmdW5jdGlvbihyZXNwb25zZSkge1xuICAgICAgICAgICAgd2luZG93LmxvY2F0aW9uID0gJy8nO1xuICAgICAgICB9KTtcbiAgICB9O1xuXG4gICAgY3RybC5pbml0ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICBjdHJsLmdldEl0ZW0oKTtcblx0fTtcblxuXHQvLyBGaXJzdCBMb2FkXG5cdGN0cmwuaW5pdCgpO1xuXG59XG5cbm1vZHVsZS5leHBvcnRzID0gaXRlbUNvbnRyb2xsZXI7XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3d3d3Jvb3QvYXBwL2NvbnRyb2xsZXJzL2l0ZW0uanNcbi8vIG1vZHVsZSBpZCA9IDRcbi8vIG1vZHVsZSBjaHVua3MgPSAwIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOyIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///4\n");

/***/ }),
/* 5 */
/* no static exports found */
/* all exports used */
/*!****************************************!*\
  !*** ./wwwroot/app/controllers/new.js ***!
  \****************************************/
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("﻿\n\nnewController.$inject = ['apiFactory'];\n\nfunction newController(api) {\n    var ctrl = this;\n\n    ctrl.product = {\n        title: '',\n        desc: '',\n        price: 0.00,\n        sku: ''\n    };\n\n    ctrl.create = function() {\n        api.createItem(ctrl.product).then(function(response) {\n        });\n    };\n}\n\nmodule.exports = newController;\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiNS5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL3d3d3Jvb3QvYXBwL2NvbnRyb2xsZXJzL25ldy5qcz9mYzkzIl0sInNvdXJjZXNDb250ZW50IjpbIu+7vyd1c2Ugc3RyaWN0JztcblxubmV3Q29udHJvbGxlci4kaW5qZWN0ID0gWydhcGlGYWN0b3J5J107XG5cbmZ1bmN0aW9uIG5ld0NvbnRyb2xsZXIoYXBpKSB7XG4gICAgdmFyIGN0cmwgPSB0aGlzO1xuXG4gICAgY3RybC5wcm9kdWN0ID0ge1xuICAgICAgICB0aXRsZTogJycsXG4gICAgICAgIGRlc2M6ICcnLFxuICAgICAgICBwcmljZTogMC4wMCxcbiAgICAgICAgc2t1OiAnJ1xuICAgIH07XG5cbiAgICBjdHJsLmNyZWF0ZSA9IGZ1bmN0aW9uKCkge1xuICAgICAgICBhcGkuY3JlYXRlSXRlbShjdHJsLnByb2R1Y3QpLnRoZW4oZnVuY3Rpb24ocmVzcG9uc2UpIHtcbiAgICAgICAgfSk7XG4gICAgfTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBuZXdDb250cm9sbGVyO1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi93d3dyb290L2FwcC9jb250cm9sbGVycy9uZXcuanNcbi8vIG1vZHVsZSBpZCA9IDVcbi8vIG1vZHVsZSBjaHVua3MgPSAwIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Iiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///5\n");

/***/ }),
/* 6 */
/* no static exports found */
/* all exports used */
/*!**************************************!*\
  !*** ./wwwroot/app/factories/api.js ***!
  \**************************************/
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("﻿\n\n\napiFactory.$inject = ['$http', '$q'];\n\nfunction apiFactory($http, $q) {\n\n\tvar self = this;\n\n    var service = {};\n\n    // API_URL - defined in webpack.config.js\n    self.baseUrl = \"http://192.168.1.15:3000/shopify?path=\";\n    self.clearLocationTimeout = 0;\n\n\tvar _callApi = function (url, data, method) {\n\n\t\tif (method === undefined) {\n\t\t\tmethod = 'GET';\n\t\t}\n\n\t\tvar httpParams = {\n\t\t\tmethod: method,\n\t\t\turl: self.baseUrl + url\n\t\t};\n\n\n        if (data !== undefined) {\n            if (method === 'GET') {\n                httpParams.params = data;\n            } else {\n                httpParams.data = data;\n            }\n\t\t}\n\n\t\tvar deferred = $q.defer();\n\n        $http(httpParams).then(function (response) {\n            deferred.resolve(response);\n        }, function (response) {\n            deferred.reject(response);\n        });\n\n\t\treturn deferred.promise;\n    };\n\n    function getTime(dt, start) {\n        //should be using moment of date-fns for this\n        var tz = dt.getTimezoneOffset() / 60 ;\n        var time = dt.getFullYear() + '/';\n        time += (dt.getMonth() + 1 < 10 ? '0' : '') + (dt.getMonth() + 1) + '/';\n        time += (dt.getDate() < 10 ? '0': '') + dt.getDate();\n        time += 'T' + (start ? '00:00:00' : '23:59:59');\n        time +=  (tz > 0 ? '-' : '+') + tz + ':00'; // this will break for .5 time zones.\n        return time;\n    }\n\n\tservice.getProducts = function (filters) {\n       console.log(filters); \n        var search = {};\n\n        if(filters.title !== '') {\n            search.title = filters.title;\n        }\n\n        if(filters.startDate !== null && filters.startDate !== '') {\n            search.updated_at_min = getTime(filters.startDate, true);\n        }\n\n        if(filters.endDate !== null && filters.endDate !== '') {\n            search.updated_at_max = getTime(filters.endDate, false);\n        }\n\n\n\t\treturn _callApi('products.json', search);\n\t};\n\n\tservice.getItem = function (id) {\n\t\treturn _callApi('products/' + id + '.json');\n\t};\n\n\tservice.createItem = function (product) {\n\t\treturn _callApi('products.json', product, 'POST');\n\t};\n\n\tservice.updateItem = function (id, product) {\n\t\treturn _callApi('products/' + id + '.json', product, 'PUT');\n\t};\n\n\tservice.removeItem = function (id) {\n\t\treturn _callApi('products/' + id + '.json', undefined, 'DELETE');\n\t};\n\n\treturn service;\n}\n\nmodule.exports = apiFactory;\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiNi5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL3d3d3Jvb3QvYXBwL2ZhY3Rvcmllcy9hcGkuanM/MzhkNiJdLCJzb3VyY2VzQ29udGVudCI6WyLvu78ndXNlIHN0cmljdCc7XG5cblxuYXBpRmFjdG9yeS4kaW5qZWN0ID0gWyckaHR0cCcsICckcSddO1xuXG5mdW5jdGlvbiBhcGlGYWN0b3J5KCRodHRwLCAkcSkge1xuXG5cdHZhciBzZWxmID0gdGhpcztcblxuICAgIHZhciBzZXJ2aWNlID0ge307XG5cbiAgICAvLyBBUElfVVJMIC0gZGVmaW5lZCBpbiB3ZWJwYWNrLmNvbmZpZy5qc1xuICAgIHNlbGYuYmFzZVVybCA9IEFQSV9VUkw7XG4gICAgc2VsZi5jbGVhckxvY2F0aW9uVGltZW91dCA9IDA7XG5cblx0dmFyIF9jYWxsQXBpID0gZnVuY3Rpb24gKHVybCwgZGF0YSwgbWV0aG9kKSB7XG5cblx0XHRpZiAobWV0aG9kID09PSB1bmRlZmluZWQpIHtcblx0XHRcdG1ldGhvZCA9ICdHRVQnO1xuXHRcdH1cblxuXHRcdHZhciBodHRwUGFyYW1zID0ge1xuXHRcdFx0bWV0aG9kOiBtZXRob2QsXG5cdFx0XHR1cmw6IHNlbGYuYmFzZVVybCArIHVybFxuXHRcdH07XG5cblxuICAgICAgICBpZiAoZGF0YSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBpZiAobWV0aG9kID09PSAnR0VUJykge1xuICAgICAgICAgICAgICAgIGh0dHBQYXJhbXMucGFyYW1zID0gZGF0YTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgaHR0cFBhcmFtcy5kYXRhID0gZGF0YTtcbiAgICAgICAgICAgIH1cblx0XHR9XG5cblx0XHR2YXIgZGVmZXJyZWQgPSAkcS5kZWZlcigpO1xuXG4gICAgICAgICRodHRwKGh0dHBQYXJhbXMpLnRoZW4oZnVuY3Rpb24gKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICBkZWZlcnJlZC5yZXNvbHZlKHJlc3BvbnNlKTtcbiAgICAgICAgfSwgZnVuY3Rpb24gKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICBkZWZlcnJlZC5yZWplY3QocmVzcG9uc2UpO1xuICAgICAgICB9KTtcblxuXHRcdHJldHVybiBkZWZlcnJlZC5wcm9taXNlO1xuICAgIH07XG5cbiAgICBmdW5jdGlvbiBnZXRUaW1lKGR0LCBzdGFydCkge1xuICAgICAgICAvL3Nob3VsZCBiZSB1c2luZyBtb21lbnQgb2YgZGF0ZS1mbnMgZm9yIHRoaXNcbiAgICAgICAgdmFyIHR6ID0gZHQuZ2V0VGltZXpvbmVPZmZzZXQoKSAvIDYwIDtcbiAgICAgICAgdmFyIHRpbWUgPSBkdC5nZXRGdWxsWWVhcigpICsgJy8nO1xuICAgICAgICB0aW1lICs9IChkdC5nZXRNb250aCgpICsgMSA8IDEwID8gJzAnIDogJycpICsgKGR0LmdldE1vbnRoKCkgKyAxKSArICcvJztcbiAgICAgICAgdGltZSArPSAoZHQuZ2V0RGF0ZSgpIDwgMTAgPyAnMCc6ICcnKSArIGR0LmdldERhdGUoKTtcbiAgICAgICAgdGltZSArPSAnVCcgKyAoc3RhcnQgPyAnMDA6MDA6MDAnIDogJzIzOjU5OjU5Jyk7XG4gICAgICAgIHRpbWUgKz0gICh0eiA+IDAgPyAnLScgOiAnKycpICsgdHogKyAnOjAwJzsgLy8gdGhpcyB3aWxsIGJyZWFrIGZvciAuNSB0aW1lIHpvbmVzLlxuICAgICAgICByZXR1cm4gdGltZTtcbiAgICB9XG5cblx0c2VydmljZS5nZXRQcm9kdWN0cyA9IGZ1bmN0aW9uIChmaWx0ZXJzKSB7XG4gICAgICAgY29uc29sZS5sb2coZmlsdGVycyk7IFxuICAgICAgICB2YXIgc2VhcmNoID0ge307XG5cbiAgICAgICAgaWYoZmlsdGVycy50aXRsZSAhPT0gJycpIHtcbiAgICAgICAgICAgIHNlYXJjaC50aXRsZSA9IGZpbHRlcnMudGl0bGU7XG4gICAgICAgIH1cblxuICAgICAgICBpZihmaWx0ZXJzLnN0YXJ0RGF0ZSAhPT0gbnVsbCAmJiBmaWx0ZXJzLnN0YXJ0RGF0ZSAhPT0gJycpIHtcbiAgICAgICAgICAgIHNlYXJjaC51cGRhdGVkX2F0X21pbiA9IGdldFRpbWUoZmlsdGVycy5zdGFydERhdGUsIHRydWUpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYoZmlsdGVycy5lbmREYXRlICE9PSBudWxsICYmIGZpbHRlcnMuZW5kRGF0ZSAhPT0gJycpIHtcbiAgICAgICAgICAgIHNlYXJjaC51cGRhdGVkX2F0X21heCA9IGdldFRpbWUoZmlsdGVycy5lbmREYXRlLCBmYWxzZSk7XG4gICAgICAgIH1cblxuXG5cdFx0cmV0dXJuIF9jYWxsQXBpKCdwcm9kdWN0cy5qc29uJywgc2VhcmNoKTtcblx0fTtcblxuXHRzZXJ2aWNlLmdldEl0ZW0gPSBmdW5jdGlvbiAoaWQpIHtcblx0XHRyZXR1cm4gX2NhbGxBcGkoJ3Byb2R1Y3RzLycgKyBpZCArICcuanNvbicpO1xuXHR9O1xuXG5cdHNlcnZpY2UuY3JlYXRlSXRlbSA9IGZ1bmN0aW9uIChwcm9kdWN0KSB7XG5cdFx0cmV0dXJuIF9jYWxsQXBpKCdwcm9kdWN0cy5qc29uJywgcHJvZHVjdCwgJ1BPU1QnKTtcblx0fTtcblxuXHRzZXJ2aWNlLnVwZGF0ZUl0ZW0gPSBmdW5jdGlvbiAoaWQsIHByb2R1Y3QpIHtcblx0XHRyZXR1cm4gX2NhbGxBcGkoJ3Byb2R1Y3RzLycgKyBpZCArICcuanNvbicsIHByb2R1Y3QsICdQVVQnKTtcblx0fTtcblxuXHRzZXJ2aWNlLnJlbW92ZUl0ZW0gPSBmdW5jdGlvbiAoaWQpIHtcblx0XHRyZXR1cm4gX2NhbGxBcGkoJ3Byb2R1Y3RzLycgKyBpZCArICcuanNvbicsIHVuZGVmaW5lZCwgJ0RFTEVURScpO1xuXHR9O1xuXG5cdHJldHVybiBzZXJ2aWNlO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGFwaUZhY3Rvcnk7XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3d3d3Jvb3QvYXBwL2ZhY3Rvcmllcy9hcGkuanNcbi8vIG1vZHVsZSBpZCA9IDZcbi8vIG1vZHVsZSBjaHVua3MgPSAwIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTsiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///6\n");

/***/ }),
/* 7 */
/* no static exports found */
/* all exports used */
/*!*********************************************!*\
  !*** ./wwwroot/app/controllers/products.js ***!
  \*********************************************/
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("﻿\n\nproductsController.$inject = ['apiFactory'];\n\nfunction productsController(api) {\n    var ctrl = this;\n\n    ctrl.filters = {\n        title: '',\n        startDate: '',\n        endDate: ''\n    }\n\n\tctrl.getProducts = function () {\n        return api.getProducts(ctrl.filters).then(function (response) {\n            ctrl.products = response.data.products;\n        });\n\t};\n\n\n    ctrl.init = function () {\n        ctrl.getProducts();\n\t};\n\n\t// First Load\n\tctrl.init();\n\n}\n\nmodule.exports = productsController;\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiNy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL3d3d3Jvb3QvYXBwL2NvbnRyb2xsZXJzL3Byb2R1Y3RzLmpzPzczOTMiXSwic291cmNlc0NvbnRlbnQiOlsi77u/J3VzZSBzdHJpY3QnO1xuXG5wcm9kdWN0c0NvbnRyb2xsZXIuJGluamVjdCA9IFsnYXBpRmFjdG9yeSddO1xuXG5mdW5jdGlvbiBwcm9kdWN0c0NvbnRyb2xsZXIoYXBpKSB7XG4gICAgdmFyIGN0cmwgPSB0aGlzO1xuXG4gICAgY3RybC5maWx0ZXJzID0ge1xuICAgICAgICB0aXRsZTogJycsXG4gICAgICAgIHN0YXJ0RGF0ZTogJycsXG4gICAgICAgIGVuZERhdGU6ICcnXG4gICAgfVxuXG5cdGN0cmwuZ2V0UHJvZHVjdHMgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiBhcGkuZ2V0UHJvZHVjdHMoY3RybC5maWx0ZXJzKS50aGVuKGZ1bmN0aW9uIChyZXNwb25zZSkge1xuICAgICAgICAgICAgY3RybC5wcm9kdWN0cyA9IHJlc3BvbnNlLmRhdGEucHJvZHVjdHM7XG4gICAgICAgIH0pO1xuXHR9O1xuXG5cbiAgICBjdHJsLmluaXQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGN0cmwuZ2V0UHJvZHVjdHMoKTtcblx0fTtcblxuXHQvLyBGaXJzdCBMb2FkXG5cdGN0cmwuaW5pdCgpO1xuXG59XG5cbm1vZHVsZS5leHBvcnRzID0gcHJvZHVjdHNDb250cm9sbGVyO1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi93d3dyb290L2FwcC9jb250cm9sbGVycy9wcm9kdWN0cy5qc1xuLy8gbW9kdWxlIGlkID0gN1xuLy8gbW9kdWxlIGNodW5rcyA9IDAiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTsiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///7\n");

/***/ })
/******/ ]);
//# sourceMappingURL=app.js.map