﻿'use strict';


apiFactory.$inject = ['$http', '$q'];

function apiFactory($http, $q) {

	var self = this;

    var service = {};

    // API_URL - defined in webpack.config.js
    self.baseUrl = API_URL;
    self.clearLocationTimeout = 0;

	var _callApi = function (url, data, method) {

		if (method === undefined) {
			method = 'GET';
		}

		var httpParams = {
			method: method,
			url: self.baseUrl + url
		};


        if (data !== undefined) {
            if (method === 'GET') {
                httpParams.params = data;
            } else {
                httpParams.data = data;
            }
		}

		var deferred = $q.defer();

        $http(httpParams).then(function (response) {
            deferred.resolve(response);
        }, function (response) {
            deferred.reject(response);
        });

		return deferred.promise;
    };

    function getTime(dt, start) {
        //should be using moment of date-fns for this
        var tz = dt.getTimezoneOffset() / 60 ;
        var time = dt.getFullYear() + '/';
        time += (dt.getMonth() + 1 < 10 ? '0' : '') + (dt.getMonth() + 1) + '/';
        time += (dt.getDate() < 10 ? '0': '') + dt.getDate();
        time += 'T' + (start ? '00:00:00' : '23:59:59');
        time +=  (tz > 0 ? '-' : '+') + tz + ':00'; // this will break for .5 time zones.
        return time;
    }

	service.getProducts = function (filters) {
       console.log(filters); 
        var search = {};

        if(filters.title !== '') {
            search.title = filters.title;
        }

        if(filters.startDate !== null && filters.startDate !== '') {
            search.updated_at_min = getTime(filters.startDate, true);
        }

        if(filters.endDate !== null && filters.endDate !== '') {
            search.updated_at_max = getTime(filters.endDate, false);
        }


		return _callApi('products.json', search);
	};

	service.getItem = function (id) {
		return _callApi('products/' + id + '.json');
	};

	service.createItem = function (product) {
		return _callApi('products.json', product, 'POST');
	};

	service.updateItem = function (id, product) {
		return _callApi('products/' + id + '.json', product, 'PUT');
	};

	service.removeItem = function (id) {
		return _callApi('products/' + id + '.json', undefined, 'DELETE');
	};

	return service;
}

module.exports = apiFactory;
