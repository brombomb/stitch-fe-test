﻿'use strict';

newController.$inject = ['apiFactory'];

function newController(api) {
    var ctrl = this;

    ctrl.product = {
        title: '',
        desc: '',
        price: 0.00,
        sku: ''
    };

    ctrl.create = function() {
        api.createItem(ctrl.product).then(function(response) {
        });
    };
}

module.exports = newController;
