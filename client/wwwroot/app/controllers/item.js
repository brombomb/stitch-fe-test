﻿'use strict';

itemController.$inject = ['apiFactory', '$location'];

function itemController(api, $location) {
    var ctrl = this;

    var params = $location.search();
    ctrl.pid = params.pid;

	ctrl.getItem = function () {
        return api.getItem(ctrl.pid).then(function (response) {
            ctrl.item = response.data.product;
        });
	};

    ctrl.update = function() {
        var update = {
            product: ctrl.item
        };

        // was going to do inventory but it's be deprecated and moved to a separate API route.
        return api.updateItem(ctrl.pid, update).then(function (response) {
            ctrl.item = response.data.product;
        });
    };


    ctrl.delete = function() {
        api.removeItem(ctrl.pid).then(function(response) {
            window.location = '/';
        });
    };

    ctrl.init = function () {
        ctrl.getItem();
	};

	// First Load
	ctrl.init();

}

module.exports = itemController;
