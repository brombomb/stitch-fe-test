﻿'use strict';

angular.module('stitchApp')
.controller('productsController', require('./products.js'))
.controller('newController', require('./new.js'))
.controller('itemController', require('./item.js'));
