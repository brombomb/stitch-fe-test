﻿'use strict';

productsController.$inject = ['apiFactory'];

function productsController(api) {
    var ctrl = this;

    ctrl.filters = {
        title: '',
        startDate: '',
        endDate: ''
    }

	ctrl.getProducts = function () {
        return api.getProducts(ctrl.filters).then(function (response) {
            ctrl.products = response.data.products;
        });
	};


    ctrl.init = function () {
        ctrl.getProducts();
	};

	// First Load
	ctrl.init();

}

module.exports = productsController;
