﻿angular.module('stitchApp', ['ui.router'])
    .config(function ($locationProvider, $stateProvider, $httpProvider) {

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });

        var states = [
            {
                name: 'products',
                url: '/',
                templateUrl: 'products.html'
            },
            {
                name: 'product',
                url: '/product?pid',
                templateUrl: 'item.html'
            },
            {
                name: 'new',
                url: '/new',
                templateUrl: 'new.html'
            }
        ];

        states.forEach(function (state) {
            $stateProvider.state(state);
        });

    })

require('./factories');
//require('./components');
require('./controllers');
//require('./filters');
